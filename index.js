const
    { version } = require('./package.json'),
    http = require('http');

const servers = {};

module.exports = {
    /**
     * @function remote
     * @description Create HTTP servers for remote Thornhill OS code execution
     * @param Window
     * @param Ouput
     * @param _
     * @param help
     * @param portToOpen - Port for new server to listen to
     * @param portToClose - Port from existing server to stop listening to
     * @param closeAll - Stop all listening servers
     */
    default: async ({ Window, Ouput }, _, { help, open: portToOpen, close: portToClose, closeAll }) => {
        let res = '';
        if(help){
            return `remote v${version}`
               + `\nUsage : remote [--open <port>]`
               + `\n        remote [--close <port>]`
               + `\n        remote [--close-all]`;
        }
        if(portToOpen){
            if(servers[portToOpen])
                res += `Port ${portToOpen} already open.\n`;
            else {
                const server = http.createServer((req, res) => {
                    if(req.method === 'POST'){
                        let body = '';
                        req.on('data', chunk => body += chunk.toString());
                        req.on('end', () => {
                            try {
                                const _res = eval(body);
                                res.writeHead(200);
                                res.end(_res && _res.toString());
                            } catch(error){
                                res.writeHead(400);
                                res.end(error.stack);
                            }
                        });
                    }
                    else {
                        res.writeHead(405);
                        res.end();
                    }
                });
                await new Promise(resolve => server.listen(portToOpen, () => resolve()));
                servers[portToOpen] = server;
                res += `Port ${portToOpen} open.\n`;
            }
        }
        if(portToClose){
            if(servers[portToClose]){
                await new Promise(resolve => servers[portToClose].close(resolve));
                delete servers[portToClose];
                res += `Port ${portToClose} closed.\n`;
            }
            else res += `Port ${portToClose} wasn't open.\n`;
        }
        if(closeAll){
            for(const [port, server] of Object.entries(servers)){
                await new Promise(resolve => server.close(resolve));
                delete servers[port];
                res += `Port ${port} closed.\n`;
            }
        }
        return res.trim();
    }
};